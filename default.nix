{ # Use master branch of the overlay by default
  mozillaOverlay ? import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz),
  rustManifest ? builtins.fetchurl "https://static.rust-lang.org/dist/channel-rust-nightly.toml"
}:

let
  pkgs = import <nixpkgs> { overlays = [ mozillaOverlay ]; };
in
with pkgs;
let
  targets = [
    "x86_64-unknown-linux-gnu"
    "thumbv6m-none-eabi"
    "thumbv7m-none-eabi"
    "thumbv7em-none-eabi"
    "thumbv7em-none-eabihf"
  ];
  rust =
    rustChannelOfTargets "nightly" null targets;
  rustPlatform = recurseIntoAttrs (makeRustPlatform {
    rustc = rust;
    cargo = rust;
  });

  openocd =
    stdenv.mkDerivation {
      name = "openocd-stlink-blackpill";
      buildInputs = [
        pkgs.openocd
        makeWrapper
      ];
      src = ./src;
      noBuild = true;
      installPhase =
        let
          openOcdFlags = [
            "-f" "${pkgs.openocd}/share/openocd/scripts/interface/stlink-v2-1.cfg"
            "-f" "${pkgs.openocd}/share/openocd/scripts/target/stm32f1x.cfg"
            "-c" "init"
          ];
        in ''
          mkdir -p $out/bin

          makeWrapper ${pkgs.openocd}/bin/openocd $out/bin/openocd-stlink-blackpill \
            --add-flags "${lib.escapeShellArgs openOcdFlags}"
        '';
    };
in {
  inherit pkgs rustPlatform openocd;
}
