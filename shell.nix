let
  project = import ./default.nix {};
in
with project.pkgs;
stdenv.mkDerivation {
  name = "project-env";
  buildInputs = with project.rustPlatform.rust; [
    rustc cargo gdb project.openocd
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;

  shellHook = ''
    echo "Starting openocd…"
    ${project.openocd}/bin/openocd-stlink-blackpill -c "reset init" &

    # Let openocd output scroll by
    sleep 1

    echo "Run 'cargo run --release'"
  '';
}
