#![no_std]
#![no_main]
// #![deny(warnings, unused)]

#[allow(unused_extern_crates)]
extern crate panic_abort;

// use cortex_m::asm::{wfi, nop};
use cortex_m_rt::entry;
use embedded_hal::{
    digital::OutputPin,
    spi::MODE_0,
    blocking::spi::Write as SpiWrite,
//     watchdog::{WatchdogEnable, Watchdog},
};
use stm32f1xx_hal::{
    flash::FlashExt,
    rcc::RccExt,
    gpio::GpioExt,
    afio::AfioExt,
    time::U32Ext,
    spi::Spi,
    stm32::{CorePeripherals, Peripherals},
};


pub struct PwmGraphics {
    t: u8,
}

impl PwmGraphics {
    pub fn new() -> Self {
        PwmGraphics { t: 0 }
    }

    pub fn generate_frame(&mut self, rgb: &[[u8; 3]; 8]) -> [u8; 3] {
        let mut bgr = [0u8; 3];

        for i in 0..bgr.len() {
            for x in 0..rgb.len() {
                if self.t < rgb[x][i] {
                    bgr[2 - i] |= 1 << x;
                }
            }
        }
        
        self.t += 1;
        bgr
    }
}

/// Initialization and main loop
#[entry]
fn main() -> ! {
    let mut cp = CorePeripherals::take().unwrap();
    cp.SCB.enable_icache();
    cp.SCB.enable_dcache(&mut cp.CPUID);

    let dp = Peripherals::take().unwrap();
    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();
    let clocks = rcc.cfgr
        .use_hse(8.mhz())
        .sysclk(72.mhz())
        .hclk(72.mhz())
        .pclk1(36.mhz())
        .pclk2(72.mhz())
        .freeze(&mut flash.acr);

    // let mut wd = IndependentWatchdog::new(dp.IWDG);
    // wd.start(1000u32.ms());
    // wd.feed();

    let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);
    let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);

    let mut led = gpiob.pb12.into_push_pull_output(&mut gpiob.crh);

    // (srclk) violet, nucleo: D13
    let sck = gpioa.pa5.into_alternate_push_pull(&mut gpioa.crl);
    // not connected
    let miso = gpioa.pa6.into_floating_input(&mut gpioa.crl);
    // (serin) blue, nucleo: D11
    let mosi = gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl);
    let pins = (sck, miso, mosi);
    let mut parts = dp.AFIO.constrain(&mut rcc.apb2);
    let mut spi = Spi::spi1(dp.SPI1, pins, &mut parts.mapr, MODE_0, 36u32.mhz(), clocks, &mut rcc.apb2);

    // (rclk) brown, nucleo: D8
    let mut rclk = gpioa.pa4.into_push_pull_output(&mut gpioa.crl);
    // (srclr) green, nucleo: D9
    let mut srclr = gpioa.pa3.into_push_pull_output(&mut gpioa.crl);
    
    // timer::setup(cp.SYST, clocks);

    srclr.set_high();
    rclk.set_low();
    let mut pg = PwmGraphics::new();
    let mut t = 0usize;
    loop {
        let mut frame = [[0u8; 3]; 8];
        for x in 0..frame.len() {
            frame[x][0] = (t >> 10) as u8;
            frame[x][1] = ((t >> 10) + (x << 1)) as u8;
            frame[x][2] = ((t >> 9) - (x << 2)) as u8;
        }
        let data = pg.generate_frame(&frame);
        spi.write(&data)
            .unwrap();

        rclk.set_high();
        // for _ in 0..200 {
        //     nop();
        // }
        rclk.set_low();

        t += 1;
        if (t / 1_000) % 2 == 0 {
            led.set_high();
        } else {
            led.set_low();
        }
    //     // for _ in 0..100000 {
    //     //     nop();
    //     // }
        
    //     // Update watchdog
    //     wd.feed();

    }
}
